package com.ruoyi.dataSource2.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.dataSource2.domain.SysDept2;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
@DS("dataSource2")
public interface SysDept2Mapper extends BaseMapper<SysDept2>
{

}
