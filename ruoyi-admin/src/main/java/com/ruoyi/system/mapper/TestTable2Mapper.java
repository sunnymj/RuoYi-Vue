package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TestTable2;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
public interface TestTable2Mapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public TestTable2 selectTestTable2ById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param testTable2 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TestTable2> selectTestTable2List(TestTable2 testTable2);

    /**
     * 新增【请填写功能名称】
     * 
     * @param testTable2 【请填写功能名称】
     * @return 结果
     */
    public int insertTestTable2(TestTable2 testTable2);

    /**
     * 修改【请填写功能名称】
     * 
     * @param testTable2 【请填写功能名称】
     * @return 结果
     */
    public int updateTestTable2(TestTable2 testTable2);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTestTable2ById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTestTable2ByIds(Long[] ids);
}
