package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.TestTable5;

/**
 * 测试3Mapper接口
 *
 * @author ruoyi
 * @date 2024-06-28
 */
public interface TestTable5Mapper extends BaseMapper<TestTable5> {
    /**
     * 查询测试3
     *
     * @param id 测试3主键
     * @return 测试3
     */
    public TestTable5 selectTestTable5ById(Long id);

    /**
     * 查询测试3列表
     *
     * @param testTable5 测试3
     * @return 测试3集合
     */
    public List<TestTable5> selectTestTable5List(TestTable5 testTable5);

    /**
     * 新增测试3
     *
     * @param testTable5 测试3
     * @return 结果
     */
    public int insertTestTable5(TestTable5 testTable5);

    /**
     * 修改测试3
     *
     * @param testTable5 测试3
     * @return 结果
     */
    public int updateTestTable5(TestTable5 testTable5);

    /**
     * 删除测试3
     *
     * @param id 测试3主键
     * @return 结果
     */
    public int deleteTestTable5ById(Long id);

    /**
     * 批量删除测试3
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTestTable5ByIds(Long[] ids);
}