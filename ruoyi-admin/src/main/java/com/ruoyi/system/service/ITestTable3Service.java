package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TestTable3;

/**
 * 测试3Service接口
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
public interface ITestTable3Service 
{
    /**
     * 查询测试3
     * 
     * @param id 测试3主键
     * @return 测试3
     */
    public TestTable3 selectTestTable3ById(Long id);

    /**
     * 查询测试3列表
     * 
     * @param testTable3 测试3
     * @return 测试3集合
     */
    public List<TestTable3> selectTestTable3List(TestTable3 testTable3);

    /**
     * 新增测试3
     * 
     * @param testTable3 测试3
     * @return 结果
     */
    public int insertTestTable3(TestTable3 testTable3);

    /**
     * 修改测试3
     * 
     * @param testTable3 测试3
     * @return 结果
     */
    public int updateTestTable3(TestTable3 testTable3);

    /**
     * 批量删除测试3
     * 
     * @param ids 需要删除的测试3主键集合
     * @return 结果
     */
    public int deleteTestTable3ByIds(Long[] ids);

    /**
     * 删除测试3信息
     * 
     * @param id 测试3主键
     * @return 结果
     */
    public int deleteTestTable3ById(Long id);
}
