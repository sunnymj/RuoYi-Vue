package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TestTable3Mapper;
import com.ruoyi.system.domain.TestTable3;
import com.ruoyi.system.service.ITestTable3Service;

/**
 * 测试3Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
@Service
public class TestTable3ServiceImpl implements ITestTable3Service 
{
    @Autowired
    private TestTable3Mapper testTable3Mapper;

    /**
     * 查询测试3
     * 
     * @param id 测试3主键
     * @return 测试3
     */
    @Override
    public TestTable3 selectTestTable3ById(Long id)
    {
        return testTable3Mapper.selectTestTable3ById(id);
    }

    /**
     * 查询测试3列表
     * 
     * @param testTable3 测试3
     * @return 测试3
     */
    @Override
    public List<TestTable3> selectTestTable3List(TestTable3 testTable3)
    {
        return testTable3Mapper.selectTestTable3List(testTable3);
    }

    /**
     * 新增测试3
     * 
     * @param testTable3 测试3
     * @return 结果
     */
    @Override
    public int insertTestTable3(TestTable3 testTable3)
    {
        testTable3.setCreateTime(DateUtils.getNowDate());
        return testTable3Mapper.insertTestTable3(testTable3);
    }

    /**
     * 修改测试3
     * 
     * @param testTable3 测试3
     * @return 结果
     */
    @Override
    public int updateTestTable3(TestTable3 testTable3)
    {
        return testTable3Mapper.updateTestTable3(testTable3);
    }

    /**
     * 批量删除测试3
     * 
     * @param ids 需要删除的测试3主键
     * @return 结果
     */
    @Override
    public int deleteTestTable3ByIds(Long[] ids)
    {
        return testTable3Mapper.deleteTestTable3ByIds(ids);
    }

    /**
     * 删除测试3信息
     * 
     * @param id 测试3主键
     * @return 结果
     */
    @Override
    public int deleteTestTable3ById(Long id)
    {
        return testTable3Mapper.deleteTestTable3ById(id);
    }
}
