package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TestTable5Mapper;
import com.ruoyi.system.domain.TestTable5;
import com.ruoyi.system.service.ITestTable5Service;

/**
 * 测试3Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-28
 */
@Service
public class TestTable5ServiceImpl extends ServiceImpl<TestTable5Mapper, TestTable5> implements ITestTable5Service {
    @Autowired
    private TestTable5Mapper testTable5Mapper;

    /**
     * 查询测试3
     *
     * @param id 测试3主键
     * @return 测试3
     */
    @Override
    public TestTable5 selectTestTable5ById(Long id) {
        return testTable5Mapper.selectTestTable5ById(id);
    }

    /**
     * 查询测试3列表
     *
     * @param testTable5 测试3
     * @return 测试3
     */
    @Override
    public List<TestTable5> selectTestTable5List(TestTable5 testTable5) {
        return testTable5Mapper.selectTestTable5List(testTable5);
    }

    /**
     * 新增测试3
     *
     * @param testTable5 测试3
     * @return 结果
     */
    @Override
    public int insertTestTable5(TestTable5 testTable5) {
        testTable5.setCreateTime(DateUtils.getNowDate());
        return testTable5Mapper.insertTestTable5(testTable5);
    }

    /**
     * 修改测试3
     *
     * @param testTable5 测试3
     * @return 结果
     */
    @Override
    public int updateTestTable5(TestTable5 testTable5) {
        return testTable5Mapper.updateTestTable5(testTable5);
    }

    /**
     * 批量删除测试3
     *
     * @param ids 需要删除的测试3主键
     * @return 结果
     */
    @Override
    public int deleteTestTable5ByIds(Long[] ids) {
        return testTable5Mapper.deleteTestTable5ByIds(ids);
    }

    /**
     * 删除测试3信息
     *
     * @param id 测试3主键
     * @return 结果
     */
    @Override
    public int deleteTestTable5ById(Long id) {
        return testTable5Mapper.deleteTestTable5ById(id);
    }
}