package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TestTable2Mapper;
import com.ruoyi.system.domain.TestTable2;
import com.ruoyi.system.service.ITestTable2Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
@Service
public class TestTable2ServiceImpl implements ITestTable2Service 
{
    @Autowired
    private TestTable2Mapper testTable2Mapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public TestTable2 selectTestTable2ById(Long id)
    {
        return testTable2Mapper.selectTestTable2ById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param testTable2 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TestTable2> selectTestTable2List(TestTable2 testTable2)
    {
        return testTable2Mapper.selectTestTable2List(testTable2);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param testTable2 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTestTable2(TestTable2 testTable2)
    {
        testTable2.setCreateTime(DateUtils.getNowDate());
        return testTable2Mapper.insertTestTable2(testTable2);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param testTable2 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTestTable2(TestTable2 testTable2)
    {
        return testTable2Mapper.updateTestTable2(testTable2);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTestTable2ByIds(Long[] ids)
    {
        return testTable2Mapper.deleteTestTable2ByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTestTable2ById(Long id)
    {
        return testTable2Mapper.deleteTestTable2ById(id);
    }
}
