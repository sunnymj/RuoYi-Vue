package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 测试3对象 test_table5
 *
 * @author ruoyi
 * @date 2024-06-28
 */
@TableName(resultMap = "com.ruoyi.system.mapper.TestTable5Mapper.TestTable5Result")
public class TestTable5 extends BaseEntity
        {
private static final long serialVersionUID = 1L;

    /** 主键 */
        @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 内容 */
            @Excel(name = "内容")
    private String content;

    /** 更新时间 */
            @JsonFormat(pattern = "yyyy-MM-dd")
            @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastUpdateTime;

    public void setId(Long id)
            {
            this.id = id;
            }

    public Long getId()
            {
            return id;
            }
    public void setContent(String content)
            {
            this.content = content;
            }

    public String getContent()
            {
            return content;
            }
    public void setLastUpdateTime(Date lastUpdateTime)
            {
            this.lastUpdateTime = lastUpdateTime;
            }

    public Date getLastUpdateTime()
            {
            return lastUpdateTime;
            }

@Override
public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("lastUpdateTime", getLastUpdateTime())
        .toString();
        }
        }