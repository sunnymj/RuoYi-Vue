package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TestTable5;
import com.ruoyi.system.service.ITestTable5Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 测试3Controller
 * 
 * @author ruoyi
 * @date 2024-06-28
 */
@RestController
@RequestMapping("/system/table5")
public class TestTable5Controller extends BaseController
{
    @Autowired
    private ITestTable5Service testTable5Service;

    /**
     * 查询测试3列表
     */
    @PreAuthorize("@ss.hasPermi('system:table5:list')")
    @GetMapping("/list")
    public TableDataInfo list(TestTable5 testTable5)
    {
        startPage();
//        List<TestTable5> listed = testTable5Service.list();
        List<TestTable5> listed = testTable5Service.lambdaQuery().eq(TestTable5::getId, 9).list();
        List<TestTable5> list = testTable5Service.selectTestTable5List(testTable5);
        return getDataTable(list);
    }

    /**
     * 导出测试3列表
     */
    @PreAuthorize("@ss.hasPermi('system:table5:export')")
    @Log(title = "测试3", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TestTable5 testTable5)
    {
        List<TestTable5> list = testTable5Service.selectTestTable5List(testTable5);
        ExcelUtil<TestTable5> util = new ExcelUtil<TestTable5>(TestTable5.class);
        util.exportExcel(response, list, "测试3数据");
    }

    /**
     * 获取测试3详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:table5:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(testTable5Service.selectTestTable5ById(id));
    }

    /**
     * 新增测试3
     */
    @PreAuthorize("@ss.hasPermi('system:table5:add')")
    @Log(title = "测试3", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TestTable5 testTable5)
    {
        return toAjax(testTable5Service.insertTestTable5(testTable5));
    }

    /**
     * 修改测试3
     */
    @PreAuthorize("@ss.hasPermi('system:table5:edit')")
    @Log(title = "测试3", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TestTable5 testTable5)
    {
        return toAjax(testTable5Service.updateTestTable5(testTable5));
    }

    /**
     * 删除测试3
     */
    @PreAuthorize("@ss.hasPermi('system:table5:remove')")
    @Log(title = "测试3", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(testTable5Service.deleteTestTable5ByIds(ids));
    }
}
