package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TestTable3;
import com.ruoyi.system.service.ITestTable3Service;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 测试3Controller
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
@RestController
@RequestMapping("/system/table3")
public class TestTable3Controller extends BaseController
{
    @Autowired
    private ITestTable3Service testTable3Service;

    /**
     * 查询测试3列表
     */
    @PreAuthorize("@ss.hasPermi('system:table3:list')")
    @GetMapping("/list")
    public TableDataInfo list(TestTable3 testTable3)
    {
        startPage();
        List<TestTable3> list = testTable3Service.selectTestTable3List(testTable3);
        return getDataTable(list);
    }

    /**
     * 导出测试3列表
     */
    @PreAuthorize("@ss.hasPermi('system:table3:export')")
    @Log(title = "测试3", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TestTable3 testTable3)
    {
        List<TestTable3> list = testTable3Service.selectTestTable3List(testTable3);
        ExcelUtil<TestTable3> util = new ExcelUtil<TestTable3>(TestTable3.class);
        util.exportExcel(response, list, "测试3数据");
    }

    /**
     * 获取测试3详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:table3:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(testTable3Service.selectTestTable3ById(id));
    }

    /**
     * 新增测试3
     */
    @PreAuthorize("@ss.hasPermi('system:table3:add')")
    @Log(title = "测试3", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TestTable3 testTable3)
    {
        return toAjax(testTable3Service.insertTestTable3(testTable3));
    }

    /**
     * 修改测试3
     */
    @PreAuthorize("@ss.hasPermi('system:table3:edit')")
    @Log(title = "测试3", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody TestTable3 testTable3)
    {
        return toAjax(testTable3Service.updateTestTable3(testTable3));
    }

    /**
     * 删除测试3
     */
    @PreAuthorize("@ss.hasPermi('system:table3:remove')")
    @Log(title = "测试3", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(testTable3Service.deleteTestTable3ByIds(ids));
    }
}
