package com.ruoyi.common.enums;

/**
 * 数据源
 * 
 * @author ruoyi
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE,

    /**
     * DCS_0
     */
    DCS_O,

    /**
     * DCS_W
     */
    DCS_W,

    /**
     * DCS_T
     */
    DCS_T,
}
