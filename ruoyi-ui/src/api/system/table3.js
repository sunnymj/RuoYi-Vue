import request from '@/utils/request'

// 查询测试3列表
export function listTable3(query) {
  return request({
    url: '/system/table3/list',
    method: 'get',
    params: query
  })
}

// 查询测试3详细
export function getTable3(id) {
  return request({
    url: '/system/table3/' + id,
    method: 'get'
  })
}

// 新增测试3
export function addTable3(data) {
  return request({
    url: '/system/table3',
    method: 'post',
    data: data
  })
}

// 修改测试3
export function updateTable3(data) {
  return request({
    url: '/system/table3',
    method: 'put',
    data: data
  })
}

// 删除测试3
export function delTable3(id) {
  return request({
    url: '/system/table3/' + id,
    method: 'delete'
  })
}
