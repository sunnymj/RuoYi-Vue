import request from '@/utils/request'

// 查询测试3列表
export function listTable5(query) {
  return request({
    url: '/system/table5/list',
    method: 'get',
    params: query
  })
}

// 查询测试3详细
export function getTable5(id) {
  return request({
    url: '/system/table5/' + id,
    method: 'get'
  })
}

// 新增测试3
export function addTable5(data) {
  return request({
    url: '/system/table5',
    method: 'post',
    data: data
  })
}

// 修改测试3
export function updateTable5(data) {
  return request({
    url: '/system/table5',
    method: 'put',
    data: data
  })
}

// 删除测试3
export function delTable5(id) {
  return request({
    url: '/system/table5/' + id,
    method: 'delete'
  })
}
