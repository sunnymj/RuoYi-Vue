import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTable2(query) {
  return request({
    url: '/system/table2/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getTable2(id) {
  return request({
    url: '/system/table2/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addTable2(data) {
  return request({
    url: '/system/table2',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateTable2(data) {
  return request({
    url: '/system/table2',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delTable2(id) {
  return request({
    url: '/system/table2/' + id,
    method: 'delete'
  })
}
